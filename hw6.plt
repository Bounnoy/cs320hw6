:- begin_tests(hw6).
:- include(hw6).

test(exeAssign, [nondet, cleanup(retractall(sym(_,_)))]) :-
  exe(assign(n,5)),
  sym(V1,5),
  sym(n,R1),
  format("~nTesting execution of assign statement: assign(n,5)~n"),
  format("Expected: ~w = ~w~nActual: ~w = ~w~n",[n,5,V1,R1]).

test(exeWhile, [nondet, cleanup(retractall(sym(_,_)))]) :-
  exe(assign(n,10)),
  sym(V1,10),
  sym(n,R1),
  exe(while(n>5,assign(n,n-1))),
  sym(V2,5),
  sym(n,R2),
  format("~nTesting execution of while statement: while(n>5, assign(n, n-1))~n"),
  format("Expected (before): ~w = ~w~nActual (before): ~w = ~w~n",[n,10,V1,R1]),
  format("Expected (after): ~w = ~w~nActual (after): ~w = ~w~n",[n,5,V2,R2]).

test(exeIf, [nondet, cleanup(retractall(sym(_,_)))]) :-
  exe(assign(n,10)),
  sym(V1,10),
  sym(n,R1),
  exe(if(n>5,assign(r,100),assign(r,999))),
  sym(V2,100),
  sym(r,R2),
  exe(if(n<5,assign(r,100),assign(r,999))),
  sym(V3,999),
  sym(r,R3),
  format("~nTesting execution of if statement: if(n>5, assign(r, 100), assign(r, 999))~n"),
  format("Expected (before): ~w = ~w~nActual (before): ~w = ~w~n",[n,10,V1,R1]),
  format("Expected (n>5): ~w = ~w~nActual (n>5): ~w = ~w~n",[r,100,V2,R2]),
  format("Expected (n<5): ~w = ~w~nActual (n<5): ~w = ~w~n",[r,999,V3,R3]).

test(exeComp, [nondet, cleanup(retractall(sym(_,_)))]) :-
  exe(comp([assign(n,10),assign(r,1)])),
  sym(V1,10),
  sym(n,R1),
  sym(V2,1),
  sym(r,R2),
  format("~nTesting execution of compound statement: comp(assign(n, 10), assign(r, 1))~n"),
  format("Expected: ~w = ~w~nActual: ~w = ~w~n",[n,10,V1,R1]),
  format("Expected: ~w = ~w~nActual: ~w = ~w~n",[r,1,V2,R2]).

test(exeOut, [nondet, cleanup(retractall(sym(_,_)))]) :-
  exe(assign(n,10)),
  format("~nTesting execution of out statement: assign(n, 10), out(n)~n"),
  format("Expected: ~w~nActual: ",['n = 10']),
  exe(out(n)), nl.

test(ppAssign, [nondet, cleanup(retractall(sym(_,_)))]) :-
  format("~nTesting pretty printing of assign statement: pps(0,assign(n,5))~n"),
  format("Expected: ~w~nActual: ",['n :- 10']),
  pps(0,assign(n,5)).

test(ppWhile, [nondet, cleanup(retractall(sym(_,_)))]) :-
  format("~nTesting pretty printing of while statement: pps(0,while(n>5,assign(n,n-1)))~n"),
  format("Expected:~nwhile n>5 do~n  n := n-1~nActual:~n"),
  pps(0,while(n>5,assign(n,n-1))).

test(ppIf, [nondet, cleanup(retractall(sym(_,_)))]) :-
  format("~nTesting pretty printing of if statement: pps(0,if(n>5,assign(n,n-1),assign(r,n)))~n"),
  format("Expected:~nif n>5 then~n  n := n-1~nelse~n  r := n~nActual:~n"),
  pps(0,if(n>5,assign(n,n-1),assign(r,n))).

test(ppComp, [nondet, cleanup(retractall(sym(_,_)))]) :-
  format("~nTesting pretty printing of compound statement: pps(0,comp([assign(n,1),assign(r,2),assign(s,5)]))~n"),
  format("Expected:~nbegin~n  n := 1~n  r := 2~n  s := 5~nend~nActual:~n"),
  pps(0,comp([assign(n,1),assign(r,2),assign(s,5)])).

test(ppOut, [nondet, cleanup(retractall(sym(_,_)))]) :-
  format("~nTesting pretty printing of out statement: pps(0,out(n))~n"),
  format("Expected: output n~nActual: "),
  pps(0,out(n)).

:- end_tests(hw6).
