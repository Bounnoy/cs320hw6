:-dynamic sym/2.

bin(E,R) :- (integer(E) -> R = E;sym(E,R)).
bin(E1+E2,R) :- bin(E1,R1), bin(E2,R2), R = R1 + R2.
bin(E1-E2,R) :- bin(E1,R1), bin(E2,R2), R = R1 - R2.
bin(E1*E2,R) :- bin(E1,R1), bin(E2,R2), R = R1 * R2.
bin(E1 mod E2,R) :- bin(E1,R1), bin(E2,R2), R = mod(R1,R2).
bin(E1//E2,R) :- bin(E1,R1), bin(E2,R2), R1 > 0, R2 > 0, mod(R1,R2) =:= 0, R = R1 // R2.
exe(assign(V,E)) :- bin(E,R), N is R, (sym(V,_) -> retract(sym(V,_));!), assert(sym(V,N)).
exe(while(E1 < E2,S)) :- bin(E1,R1), bin(E2,R2), (R1 < R2 -> exe(S), exe(while(E1 < E2,S));!).
exe(while(E1 > E2,S)) :- bin(E1,R1), bin(E2,R2), (R1 > R2 -> exe(S), exe(while(E1 > E2,S));!).
exe(if(E1 < E2, S1, S2)) :- bin(E1,R1), bin(E2,R2), R1 < R2 -> exe(S1) ; exe(S2).
exe(if(E1 > E2, S1, S2)) :- bin(E1,R1), bin(E2,R2), R1 > R2 -> exe(S1) ; exe(S2).
exe(comp([H|T])) :- exe(H), (T = [_|_] -> exe(comp(T));!).
exe(out(E)) :- sym(E,V), format("~w = ~w",[E,V]).

pps(N,assign(V,E)) :- tab(N), format("~w := ~w~n",[V,E]).
pps(N,while(E,S)) :- tab(N), format("while ~w do~n",[E]), N1 is N+2, pps(N1,S).
pps(N,if(E,S1,S2)) :- tab(N), format("if ~w then~n",[E]), N1 is N+2, pps(N1,S1), format("else~n"), pps(N1,S2).
pps(N,comp([H|T])) :- tab(N), format("begin~n"), N1 is N+2, pps(N1,H), ppc(N1,T), tab(N), format("end~n").
pps(N,out(V)) :- tab(N), format("output ~w~n",[V]).
ppc(N,[H|T]) :- pps(N,H), (T = [_|_] -> ppc(N,T);!).

factorial(R) :- R = comp([assign(n,5),assign(r,1),while(n>0,comp([assign(r,r*n),assign(n,n-1)])),out(r)]).
lcm(R) :- R = comp([assign(a1,16),assign(b1,36),assign(a2,16),assign(b2,36),assign(r,0),while(b1>0,
  comp([assign(r,a1 mod b1),assign(a1,b1),assign(b1,r)])), assign(a2,a2*b2//a1),out(a2)]).

pp(factorial) :- factorial(R), pps(0,R),!.
pp(lcm) :- lcm(R), pps(0,R),!.
ex(factorial) :- factorial(R), exe(R).
ex(lcm) :- lcm(R), exe(R).

start :- write("------------------------------"),nl,
  write("        Factorial(5)"),nl,
  write("------------------------------"),nl,
  pp(factorial),
  write("---------- Results -----------"),nl,
  ex(factorial),nl,
  write("------------------------------"),nl,
  write(" Least Common Multiple (16,36)"),nl,
  write("------------------------------"),nl,
  write("LCM Formula: (a * b) / gcd(a, b)"),nl,
  write("a1 and b1 below calculates gcd"),nl,
  write("a2 and b2 below calculates lcm"),nl,
  pp(lcm),
  write("---------- Results -----------"),nl,
  ex(lcm),nl,
  write("------------------------------"),nl.
